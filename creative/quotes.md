Canvas effect
“The person who clears the path ultimately controls its direction, just as the canvas shapes the painting.” – Ryan Holiday

Success can usually be measured by the number of uncomfortable conversations a person is willing to take.

“The moment that you feel, just possibly, you are walking down the street naked, exposing too much of your heart and your mind, and what exists on the inside, showing too much of yourself...That is the moment, you might be starting to get it right.”

“Go and make interesting mistakes, make amazing mistakes, make glorious and fantastic mistakes. Break rules. Leave the world more interesting for your being here.” 

“When you are content to be simply yourself and don't compare or compete, everyone will respect you.” - Lao Tzu

If you're offended easily, you're a bad resource allocator. It's a waste of energy and attention, which is a greater sin than wasting time.

Passion mindset is wrong. Think what you can give to the world, not what the world can give to you. That's how you make millions.
